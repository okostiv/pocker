﻿#pragma once
#define _CLASS_CARD_INCLUDED
#include "common.h"
#include "nmspc_game.h"

class NGame::CCard{
public:
	enum ECard;
	enum EDeck;
	typedef unsigned long int TValue;
private:
	TValue m_value;
	TBool m_show;
public:
	CCard();
	CCard(CCard const&&);
	CCard(ECard const, EDeck const);
	CCard(TValue const);
public:
	operator const char*() const;
public:
	ECard const get_card() const;
	EDeck const get_deck() const;

	TBool const open();
	TBool const hide();

	TBool const is_opened() const;
	TBool const is_good() const;
public:
	enum EDeck{
		deck_0  = 0x00, // empty
		deck_h1 = 0x01, // hearts
		deck_d2 = 0x02, // diamonds
		deck_c3 = 0x04, // clubs
		deck_s4 = 0x08,	// spades
		deck_t  = 0x10, // trump
		deck_test = 0xff
	};
	enum ECard{
		card_6 = 0x0010 << 5,
		card_7 = 0x0020 << 5,
		card_8 = 0x0040 << 5,
		card_9 = 0x0080 << 5,
		card_T = 0x0100 << 5,
		card_J = 0x0200 << 5,
		card_Q = 0x0400 << 5,
		card_K = 0x0800 << 5,
		card_A = 0x1000 << 5,

		card_test = 0xffff << 5
	};
};