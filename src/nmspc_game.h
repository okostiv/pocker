#pragma once
#define _NAMESPACE_GAME_INCLUDED
namespace NGame{
	typedef bool TBool;
	typedef int TSint;
	typedef unsigned TUint;

	class CCard;
	class CCombination;
	class CTable;
	class CPlayer;
};