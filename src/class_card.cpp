﻿#include "common.h"
#include "nmspc_game.h"
#include "class_card.h"
//c++
#include <string>
//c
#include <cmath>
#include <cstring>
//src
using namespace NGame;
using namespace std;

CCard::CCard():
	m_value(0UL),
	m_show(false) 
{}
CCard::CCard(CCard const&& crr_obj):
	m_value(crr_obj.m_value),
	m_show(false)
{};
CCard::CCard(ECard const c_card, EDeck const c_deck):
	m_value(0UL),
	m_show(false)
{
	do{
		double power = (log((int)c_card)+log((int)c_deck))/log(2);
		if(power != int(power))
			break;
		
		m_value = (c_card|c_deck);
	}while(false);
}
CCard::CCard(CCard::TValue const c_value):
	m_value(c_value),
	m_show(false)
{}

CCard::ECard const CCard::get_card() const{
	return ECard(m_value& card_test);
}
CCard::EDeck const CCard::get_deck() const{
	return EDeck(m_value& deck_test);
}

CCard::operator const char*()const{
	TValue value = m_value;
	char * str = new char[5];
	strcpy(str,"[");

	switch(value& ECard::card_test){
		case card_6: strcat(str, to_text(6)); break;
		case card_7: strcat(str, to_text(7)); break;
		case card_8: strcat(str, to_text(8)); break;
		case card_9: strcat(str, to_text(9)); break;	 
		case card_T: strcat(str, to_text(T)); break;
		case card_J: strcat(str, to_text(J)); break;
		case card_Q: strcat(str, to_text(Q)); break;
		case card_K: strcat(str, to_text(K)); break;
		case card_A: strcat(str, to_text(A)); break;
		default:     strcat(str, "X"       );
	}
	char h[] = {3,0, 4,0, 5,0, 6,0};
	switch(value& (EDeck::deck_test)){
		case deck_h1: strcat(str, &h[0]);break;
		case deck_d2: strcat(str, &h[2]);break;
		case deck_c3: strcat(str, &h[4]);break;
		case deck_s4: strcat(str, &h[6]);break;
		default:      strcat(str, "X"       );
	}
	strcat(str, "]");
	return str;
}

TBool const CCard::open(){
	if(!m_show)
		return m_show=true;
	else
		return false;
}
TBool const CCard::hide(){
	if(m_show)
		return ! (m_show=false);
	else
		return !true;
}
TBool const CCard::is_opened() const{
	return m_show;
}
TBool const CCard::is_good() const{
	return TBool(m_value);
}