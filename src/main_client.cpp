#include "common.h"

#include <iostream>
#include <string>
using namespace std;

#include <WinSock2.h>
#pragma comment(lib, "ws2_32.lib")

DWORD WINAPI SendThread(PVOID);
DWORD WINAPI RecieveThread(PVOID);

int main(){
	INT iResult = EXIT_FAILURE;
	do{
		if(WSAStartup(MAKEWORD(2,2), new WSADATA)){
			cout << "<client> start up failed." << endl;
			break;
		}
		
		SOCKET Sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if(Sock == INVALID_SOCKET){
			cout << "<client> failed to create socket." << endl;
			break;
		}

		PHOSTENT pHost = gethostbyname("localhost");
		if(!pHost){
			cout << "<client> failed to find host." << endl;
			break;
		}

		SOCKADDR_IN Soadin;
		Soadin.sin_family = AF_INET;
		Soadin.sin_addr.s_addr = *(PULONG)pHost->h_addr;
		Soadin.sin_port = htons(usPort);

		if(connect(Sock, (PSOCKADDR)&Soadin, sizeof(Soadin))){
			cout << "<client> failed to connect to the <server>." << endl;
			break;
		}

		HANDLE hSendThread = CreateThread(NULL, 0, SendThread, &Sock, 0, new DWORD);
		HANDLE hRecieveThread = CreateThread(NULL, 0, RecieveThread, &Sock, 0, new DWORD);
		
		WaitForMultipleObjects(1, &hRecieveThread, TRUE, INFINITE);
		
		shutdown(Sock,SD_SEND);
		closesocket(Sock);

		iResult = EXIT_SUCCESS;
	}while(FALSE);
	WSACleanup();
	system("pause");
	return iResult;
}

DWORD WINAPI SendThread(PVOID pParam){
	SOCKET& Socket = *(SOCKET*)pParam;
	while(TRUE){
		string sSend;
		getline(cin, sSend);
		send(Socket, sSend.c_str(), sSend.length()+1, 0);
		Sleep(100);
	}
	return 0;
}
DWORD WINAPI RecieveThread(PVOID pParam){
	SOCKET& Socket = *(SOCKET*)pParam;
	while(TRUE){
		char buffer[1024] = {0};
		INT iResult = recv(Socket, buffer, 1024, 0);
	    if (iResult > 0){
			cout << buffer;
		}
	    else{
			if (iResult == 0)
				cout << "Connection closed." << endl;
			else
				cout << "recv failed: " << WSAGetLastError() << "." << endl;
			break;
		}
		Sleep(100);
	}
	return 0;
}