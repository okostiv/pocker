#pragma once
#define _CRT_SECURE_NO_WARNINGS

#define _COMMON_H_INCLUDED

//#define MICRO_STR 1<<4			// 16
//#define PETTY_STR MICRO_STR<<1	// 32
//#define SMALL_STR PETTY_STR<<1	// 64
//#define PLAIN_STR SMALL_STR<<1	// 128
//#define LARGE_STR PLAIN_STR<<1	// 256
//#define GREAT_STR LARGE_STR<<1	// 512
//#define GRAND_STR GREAT_STR<<1	// 1024
//#define SUPER_STR GRAND_STR<<1    // 2048

#define usPort unsigned short(1488)
#define to_text(x) #x